import React, {Component, Fragment} from 'react';
import {Collapse, Nav, NavbarToggler, NavItem, NavLink} from "reactstrap";
import { Route, Switch, NavLink as RouterNavLink } from 'react-router-dom';

import './App.css';
import Container from "reactstrap/es/Container";
import {Navbar, NavbarBrand} from "reactstrap";
import quoteList from "./containers/quoteList";
import AddQuote from "./containers/addQuote";

const categories = {
  'mtv': 'Motivational',
  'hmr': 'Humor',
  'fms': 'Famous people'
};

class App extends Component {
  render() {
    return (
        <Fragment>
            <Navbar color="warning" light expand="md">
                <NavbarBrand>Quotes Central</NavbarBrand>
                <NavbarToggler/>
                <Collapse isOpen navbar>
                    <Nav className="ml-auto" navbar>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/" exact>Quotes</NavLink>
                        </NavItem>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/submitNewQuote">Add quote</NavLink>
                        </NavItem>
                    </Nav>
                </Collapse>
            </Navbar>
            <Container>
              <Switch>
                <Route path="/" exact component={quoteList}/>
                <Route path="/submitNewQuote" component={AddQuote}/>
                <Route path="/quotes/:categoryId" component={quoteList}/>
                <Route render={() =>  <h1>Not found</h1>}/>
              </Switch>
            </Container>
        </Fragment>
    );
  }
}

export default App;

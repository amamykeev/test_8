import React, {Component} from 'react';
import {CATEGORIES} from "../constants";
import {Col, Form, FormGroup, Label} from "reactstrap";
import Input from "reactstrap/es/Input";
import Button from "reactstrap/es/Button";

class SubmitNewQuote extends Component {

    state = {
        name: '',
        category: Object.keys(CATEGORIES)[0],
        description: ''
    };

    valueChanged = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    submitHandler = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <Form className="submitNewQuote" onSubmit={this.submitHandler}>
                <FormGroup row>
                    <Label for="category" sm={2}>Category</Label>
                    <Col sm={10}>
                        <Input type="select" name="category" id="category"
                               value={this.state.category} onChange={this.valueChanged}
                        >
                                {Object.keys(CATEGORIES).map(categoryId => (
                                    <option key={categoryId} value={categoryId}>{CATEGORIES[categoryId]}</option>
                                ))}
                        </Input>
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Label for="name" sm={2}>Enter author</Label>
                    <Col sm={10}>
                    <Input type="text" name="name" id="name"
                           value={this.state.name} onChange={this.valueChanged}
                />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="category" sm={2}>Enter description</Label>
                    <Col sm={10}>
                    <Input type="textarea" name="description"
                          value={this.state.description}
                          onChange={this.valueChanged}
                />
                    </Col>
                </FormGroup>

                <FormGroup row>
                    <Col sm={{size: 10, offset: 2}}>
                        <Button color="primary" type="submit">Save</Button>
                    </Col>
                </FormGroup>
            </Form>
        );
    }
}

export default SubmitNewQuote;
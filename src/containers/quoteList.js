import React, {Component} from 'react';
import {Button, Card, CardBody, CardColumns, CardText, CardTitle, Col, Nav, NavItem, NavLink, Row} from "reactstrap";
import { NavLink as RouterNavLink } from 'react-router-dom';
import axios from '../axios-test8js3';
import {CATEGORIES} from '../constants';

class QuoteList extends Component {
    state = {
        quotes: null
    };

    componentDidMount() {
      let url = 'quotes.json';

      axios.get('quotes.json').then(response => {
          const quotes = Object.keys(response.data).map(id => {
              return {...response.data[id], id};
          });

          this.setState({quotes});
      })
    }

    componentDidUpdate() {
        console.log(this.props.match.params);
    }



    render() {
        let quotes = null;

        if (this.state.quotes) {
            quotes = this.state.quotes.map(quote => (
                <Card key={quote.id}>
                    <CardBody>
                        <CardText>{quote.description}</CardText>
                        <CardTitle><strong>{quote.name}</strong></CardTitle>
                        <Button color="warning" style={{marginRight: '5px'}} type="Button">Edit</Button>
                        <Button color="danger" type="button">Delete</Button>
                    </CardBody>
                </Card>
        ));
        }

        return (
            <Row style={{marginTop: '15px'}}>
                <Col sm={3}>
                    <Nav vertical>
                        <NavItem>
                            <NavLink tag={RouterNavLink} to="/" exact>All quotes</NavLink>
                        </NavItem>
                        {Object.keys(CATEGORIES).map(categoryId => (
                            <NavItem key={categoryId}>
                                <NavLink
                                    tag={RouterNavLink}
                                    to={"/quotes/" + categoryId}
                                    exact
                            >
                                {CATEGORIES[categoryId]}
                            </NavLink>
                            </NavItem>
                        ))}
                    </Nav>
                </Col>
                <Col sm={9}>
                    <CardColumns>
                        {quotes}
                    </CardColumns>
                </Col>
            </Row>
        );
    }
}

export default QuoteList;
import React, {Component, Fragment} from 'react';
import axios from '../axios-test8js3';
import SubmitNewQuote from '../components/submitNewQuote';

class AddQuote extends Component {
    addQuote = quote => {
        axios.post('quotes.json', quote).then(() => {
            this.props.history.replace('/');
        });
    };

    render() {
        return (
            <Fragment>
                <h1>Add new quote</h1>
                <SubmitNewQuote onSubmit={this.addQuote} />
            </Fragment>
        );
    }
}

export default AddQuote;
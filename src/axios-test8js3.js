import axios from 'axios';
const instance = axios.create({
    baseURL: 'https://test8js3.firebaseio.com/'
});

export default instance;